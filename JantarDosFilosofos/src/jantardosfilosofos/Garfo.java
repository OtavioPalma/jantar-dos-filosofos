package jantardosfilosofos;

import java.util.concurrent.atomic.AtomicBoolean;

public class Garfo 
{
    private AtomicBoolean emUso = new AtomicBoolean(false);
    private int posicao;
    
    public Garfo(int pos)
    {
        this.posicao = pos;
    }
    
    public int getPosicao()
    {
        return this.posicao;
    }
    
    public synchronized void pegar() throws InterruptedException
    {
        while(this.emUso.get() == true)
        {
            this.wait();
        }
        System.out.println("Garfo " + this.posicao + " foi pego!");
        this.emUso.set(true);
    }
    
    public synchronized void devolver()
    {
        this.emUso.set(false);
        this.notifyAll();
        System.out.println("Garfo " + this.posicao + " devolvido!");
    }
}
