package jantardosfilosofos;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class JantarDosFilosofos 
{
    public static void main(String[] args) 
    {
        ArrayList<Filosofo> filosofos = new ArrayList<>();
        ArrayList<Garfo> garfos = new ArrayList<>();
        Garfo garfo;
        Filosofo filosofo;
        
        for(int i = 0; i < 5; i++)
            garfos.add(garfo = new Garfo(i));
        
        for(int i = 0; i < 5; i++)
        {
            if(i == 4)
                filosofos.add(filosofo = new Filosofo(garfos.get(i), garfos.get(0), i) );
            else
                filosofos.add(filosofo = new Filosofo(garfos.get(i), garfos.get(i+1), i));
            
        }
        
        ExecutorService threadExecutor = Executors.newCachedThreadPool();
        
        for(int i = 0; i < 5; i++)
            threadExecutor.execute(filosofos.get(i));
        
        threadExecutor.shutdown();
    }
    
}
