package jantardosfilosofos;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Filosofo implements Runnable
{
    private Garfo esquerdo;
    private Garfo direito;
    private int id;
    private Random random = new Random();

    public Filosofo(Garfo esquerdo, Garfo direito, int id) 
    {
        this.esquerdo = esquerdo;
        this.direito = direito;
        this.id = id;
    }
    
    public synchronized void comer() throws InterruptedException
    {
        System.out.println("Filósofo " + this.id + " comendo...");
        this.wait(random.nextInt(5000));
    }
    
    public void pensar()
    {
        System.out.println("Filósofo " + this.id + " pensando...");
        try 
        {
            Thread.sleep(random.nextInt(5000));
        }
            catch (InterruptedException ex)
            {
                System.out.println("ERROR1");
            }
    }
    
    @Override
    public void run() 
    {
        while(true)
        {
            this.pensar();
            
            try
            {
                this.esquerdo.pegar();
                System.out.println("Filósofo " + this.id + " pegou o garfo esquerdo");
                this.direito.pegar();
                System.out.println("Filósofo " + this.id + " pegou o garfo direito");
                this.comer();
            }
                catch(InterruptedException ex)
                {
                    System.out.println("ERROR2");
                }
            
            this.esquerdo.devolver();
            this.direito.devolver();
        }
    }
    
}
